from conans import ConanFile, CMake, tools
from conans.errors import ConanInvalidConfiguration


class SystemcConan(ConanFile):
    name = "SystemC"
    version = "2.3.3"
    description = """SystemC is a set of C++ classes and macros which provide
                     an event-driven simulation interface."""
    homepage = "https://www.accellera.org/"
    url = "https://gitlab.com/dxplore/conan-systemc"
    license = "Apache-2.0"
    author = "Jan Van Winkel <jan.van_winkel@dxplore.eu>"
    topics = ("simulation", "modeling", "esl", "tlm")
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "build_doc": [True, False],
        "disable_async_updates": [True, False],
        "disable_copyright_msg": [True, False],
        "disable_virtual_bind": [True, False],
        "enable_assertions": [True, False],
        "enable_immediate_self_notifications": [True, False],
        "enable_pthreads": [True, False]
    }
    default_options = {
        "shared": True,
        "build_doc": False,
        "disable_async_updates": False,
        "disable_copyright_msg": False,
        "disable_virtual_bind":  False,
        "enable_assertions": True,
        "enable_immediate_self_notifications": False,
        "enable_pthreads": False
    }
    generators = "cmake"
    exports_sources = "sc_string_view.patch"

    def configure(self):

        if tools.valid_min_cppstd(self, "17"):
            raise ConanInvalidConfiguration(
                "C++ Standard %s not supported by SystemC" %
                self.settings.compiler.cppstd)

        if not self.settings.compiler.cppstd:
            if tools.valid_min_cppstd(self, "14"):
                self.settings.compiler.cppstd = 14
            elif tools.valid_min_cppstd(self, "11"):
                self.settings.compiler.cppstd = 11
            elif tools.valid_min_cppstd(self, "98"):
                self.settings.compiler.cppstd = 98

    def source(self):
        git = tools.Git()
        git.clone(url="https://github.com/accellera-official/systemc.git",
                  branch=self.version,
                  shallow=True
                  )
        tools.replace_in_file("CMakeLists.txt",
                              "project (SystemCLanguage CXX C)",
                              """project (SystemCLanguage CXX C)
                              include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
                              conan_basic_setup()""")
        tools.patch(patch_file="sc_string_view.patch")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_SOURCE_DOCUMENTATION"] = \
            self.options.build_doc
        cmake.definitions["CMAKE_CXX_STANDARD"] = self.settings.compiler.cppstd
        cmake.definitions["DISABLE_ASYNC_UPDATES"] = \
            self.options.disable_async_updates
        cmake.definitions["DISABLE_COPYRIGHT_MESSAGE"] = \
            self.options.disable_copyright_msg
        cmake.definitions["DISABLE_VIRTUAL_BIND"] = \
            self.options.disable_virtual_bind
        cmake.definitions["ENABLE_ASSERTIONS"] = \
            self.options.enable_assertions
        cmake.definitions["ENABLE_IMMEDIATE_SELF_NOTIFICATIONS"] = \
            self.options.enable_immediate_self_notifications
        cmake.definitions["ENABLE_PTHREADS"] = self.options.enable_pthreads
        cmake.configure()
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["systemc"]

        if (self.settings.os == "Linux") and not self.options.shared:
            self.cpp_info.libs.append("pthread")
